package com.project.iot.sensorreadings.interfaces.navigation

import java.util.Timer

interface Schedulable {
    fun runTask(timer: Timer)
}
