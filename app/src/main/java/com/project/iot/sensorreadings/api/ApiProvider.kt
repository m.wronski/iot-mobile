package com.project.iot.sensorreadings.api

import com.github.kittinunf.fuel.core.FuelManager
import com.project.iot.sensorreadings.interfaces.navigation.AuthorizationProvider
import com.project.iot.sensorreadings.utils.AppConfig

class ApiProvider : AuthorizationProvider {
    private val apiClient: FuelManager = FuelManager()

    override suspend fun obtainHttpClient(): FuelManager {

        //TODO
        //apiClient.basePath = "http://10.0.2.2:8099"
        apiClient.basePath = AppConfig.instance.baseApiUrl
        apiClient.baseHeaders = mapOf(
            "Content-Type" to "application/json"
        )
        return apiClient
    }
}
