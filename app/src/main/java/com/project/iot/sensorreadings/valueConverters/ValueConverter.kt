package com.project.iot.sensorreadings.valueConverters

interface ValueConverter<in TSource, out TTarget> {
    fun convert(obj: TSource): TTarget
}
