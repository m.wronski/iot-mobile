package com.project.iot.sensorreadings.interfaces.navigation.adapters

import com.project.iot.sensorreadings.interfaces.navigation.DispatcherAdapter
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async

class AndroidDispatcherAdapter : DispatcherAdapter {
    override fun run(action: suspend () -> Unit) {
        async(UI) {
            action()
        }
    }
}
