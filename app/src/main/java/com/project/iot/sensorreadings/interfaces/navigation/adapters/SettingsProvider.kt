package com.project.iot.sensorreadings.interfaces.adapters

interface SettingsProvider {
    fun putInt(key: String, value: Int)
    fun getInt(key: String): Int?

    fun putString(key: String, value: String)
    fun getString(key: String): String?

    fun clear(key: String)

    fun <T : Any> putObject(key: String, value: T)
    fun <T : Any> getObject(key: String): T?
}
