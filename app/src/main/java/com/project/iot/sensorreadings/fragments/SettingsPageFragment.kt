package com.project.iot.sensorreadings.fragments

import android.text.TextUtils
import android.util.Patterns
import com.jakewharton.rxbinding2.widget.RxTextView
import com.project.iot.sensorreadings.R
import com.project.iot.sensorreadings.viewModels.SettingsViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.settings_page.ConfirmSettings
import kotlinx.android.synthetic.main.settings_page.ReturnToDashobard
import kotlinx.android.synthetic.main.settings_page.ServerUriEditText
import kotlinx.android.synthetic.main.settings_page.ServerUriTextInputLayout
import java.util.concurrent.TimeUnit

class SettingsPageFragment : FragmentBase<SettingsViewModel>(SettingsViewModel::class.java) {
    override val layoutResourceId: Int
        get() = R.layout.settings_page

    override fun navigatedTo() {
        viewModel.navigatedTo()
        ServerUriEditText.setText(viewModel.serverURI)
    }

    override fun initBindings() {
        ConfirmSettings.setOnClickListener { viewModel.onConfirmSettingsButtonClick() }

        RxTextView.afterTextChangeEvents(ServerUriEditText)
            .debounce(50, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (Patterns.WEB_URL.matcher(it.view().text.toString()).matches()) {
                    viewModel.serverURI = it.view().text.toString()
                    ConfirmSettings.isEnabled = true
                    ConfirmSettings.alpha = 1f
                    ServerUriTextInputLayout.isErrorEnabled = false
                } else {
                    ConfirmSettings.isEnabled = false
                    ConfirmSettings.alpha = .5f
                    ServerUriTextInputLayout.isErrorEnabled = false
                    if (!TextUtils.isEmpty(it.view().text)) {
                        ServerUriTextInputLayout.error = "Incorrect server URI"
                    }
                }
            }

        ReturnToDashobard.setOnClickListener {
            viewModel.navigateBack()
        }
    }
}
