package com.project.iot.sensorreadings.interfaces.navigation.adapters

import android.support.v4.app.Fragment
import com.project.iot.sensorreadings.interfaces.navigation.PageProvider

class CachedPageProvider<TPage : Fragment>(private val pageFactory: () -> (TPage)) :
    PageProvider<TPage> {
    private var page: TPage? = null

    override fun getPage(): TPage {
        if (page == null)
            page = pageFactory()
        return page as TPage
    }
}
