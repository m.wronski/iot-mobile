package com.project.iot.sensorreadings.interfaces.navigation

interface PageProvider<TPage> {
    fun getPage(): TPage
}
