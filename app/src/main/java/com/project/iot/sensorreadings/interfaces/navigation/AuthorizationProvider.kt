package com.project.iot.sensorreadings.interfaces.navigation

import com.github.kittinunf.fuel.core.FuelManager

interface AuthorizationProvider {
    suspend fun obtainHttpClient(): FuelManager
}
