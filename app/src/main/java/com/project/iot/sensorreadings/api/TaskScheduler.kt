package com.project.iot.sensorreadings.api

import com.project.iot.sensorreadings.interfaces.navigation.Schedulable
import java.util.Timer

class TaskScheduler(private val schedulableViewModelsSet: Set<Schedulable>) {
    private val timer = Timer()

    fun executeTasks() {
        schedulableViewModelsSet.forEach { it.runTask(timer) }
    }
}
