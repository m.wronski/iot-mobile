package com.project.iot.sensorreadings.api

import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.fuel.core.Response
import com.github.kittinunf.result.Result
import com.google.gson.reflect.TypeToken
import com.project.iot.sensorreadings.interfaces.navigation.AuthorizationProvider
import com.project.iot.sensorreadings.interfaces.navigation.api.ApiCommunicator
import com.project.iot.sensorreadings.models.enums.model.Measurement
import com.project.iot.sensorreadings.utils.GsonConvert
import java.util.logging.Logger

class MainApiCommunicator(
    private val authorizationProvider: AuthorizationProvider
) : ApiCommunicator {

    override suspend fun getMeasurements(): Result<Measurement, Exception> {
        val manager = authorizationProvider.obtainHttpClient()

        val response = manager.request(
            Method.GET,
            "/measurement"
        )
            .response()
            .handleResponse()

        response.third.fold({
            return Result.of {
                GsonConvert.deserializeObject<Measurement>(
                    it.toString(Charsets.UTF_8),
                    object : TypeToken<Measurement>() {}.type
                )
            }
        }, {
            return Result.error(it)
        })
    }

    private fun Triple<Request, Response, Result<ByteArray, FuelError>>.handleResponse(): Triple<Request, Response, Result<ByteArray, FuelError>> {
        when (this.third) {
            is Result.Failure -> {
                Logger.getLogger("${second.statusCode} Failed obtaining response from ${first.url}")
            }
        }
        return this
    }
}
