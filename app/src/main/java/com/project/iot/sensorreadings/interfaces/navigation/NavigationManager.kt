package com.project.iot.sensorreadings.interfaces.navigation

import com.project.iot.sensorreadings.models.enums.PageIndex

interface NavigationManager {
    fun navigate(page: PageIndex)
    fun goBack()
    fun wentBack()
}
